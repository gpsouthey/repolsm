import React, { useState, useEffect, useReducer } from 'react';
import { Text,
     StyleSheet, 
     View,   
     Image,
    ImageBackground,
    ScrollView,
    TextInput,
    TouchableOpacity,
    SafeAreaView,
    Alert,
    
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import DateTimePicker from '@react-native-community/datetimepicker';
import DatePicker from 'react-native-date-picker'
import AsyncStorage from '@react-native-async-storage/async-storage';
import Camera from './components/Camera';
import QuotaNamePicker from './components/QuotaNamePicker';
import AreaPicker from './components/AreaPicker';
import VesselPicker from './components/VesselPicker';
import LandingSitePicker from './components/LandingSitePicker';
import HarbourPicker from './components/HarbourPicker';
import GetLocation from 'react-native-get-location';
import Geo from './components/Geolocation';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SelectDropdown from 'react-native-select-dropdown'



const HomeScreen = (props) => {


    const [landing_slip, setlanding_slip] = useState('');
     const [landed_date, setlanded_date] = useState('');
     const [quota, setquota] = useState();
     const [quota_name, setquota_name] = useState();
     const [area, setarea] = useState();
     const [vessel, setvessel] = useState();
     const [kgs_slips, setkgs_slips] = useState();
     const [kgs_received, setkgs_received] = useState();
     
     const [traps, settraps] = useState();
     const [landing_site, setlanding_site] = useState();
     const [harbour, setharbour] = useState();
     const [gps, setgps] = useState();
     const [image, setimage] = useState();

    const [date, setDate] = useState(new Date());

    const onChange = (event, selectedDate) => {
     const currentDate = selectedDate || date;
     setDate(currentDate);

     useEffect(() => {

        retrieveUserData();
  
      },[]);


      
      

     async function storeFormData() {
        try {
            await EncryptedStorage.setItem(
                "user_data",
                JSON.stringify({
                
                        landing_slip: landing_slip,
                        landed_date: landed_date,
                        quota: quota,
                        quota_name: quota_name,
                        area: area,
                        vessel: vessel,
                        kgs_slips: kgs_slips,
                        kgs_received: kgs_received,
                        traps: traps,
                        landing_site: landing_site,
                        harbour: harbour,
                        gps: gps,
                        image: image
                        

                })
            );
    
            
            
        } catch (error) {
            // There was an error on the native side
        }


        
  async function retrieveUserData() {
    try {   
        const user_data = JSON.parse(await EncryptedStorage.getItem("user_data"));
    
        if (user_data !== undefined) {
            // Congrats! You've just retrieved your first value!
            setlanding_slip(user_data.landing_slip);
            setlanded_date(user_data.landed_date);
            setquota(user_data.quota);
            setquota_name(user_data.quota_name);
            setarea(user_data.area);
            setvessel(user_data.vessel);
            setkgs_slips(user_data.kgs_slips);
            setkgs_received(user_data.kgs_received);
            settraps(user_data.traps);
            setharbour(user_data.harbour);
            setgps(user_data.gps);
            setimage(user_data.image);
            
            
        }
    } catch (error) {
        // There was an error on the native side
    }
}


    }

    };

    const areaData = ['3', '6','8', '11', '12'];
    const vessselData = ['PEARL', 'WINRAY','ALFA ', 'OCEAN12', 'ABIGAIL'];
    const harbourData = ['Kleinmond', 'Gordonsbay Harbour','Elandsbaai', 'Lambertsbay', 'Kalkbay Harbour'];
    const quotaData = ['FJ SWARTZ (IKM)', 'WC JANUARY  (IKM)', 'GM LEIBRANDT (IKM)', 'WC JANUARY  (IKM) ', 'NA GQOSHA (IKM)'];
    const landingsiteData = ['4', '7','10', '18', '30'];


    function post_landing_data(loading_data){

        

        return new Promise(function(resolve,reject){
        try{

        fetch('https://lms-api.isnet.co.za/receive_catch', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(loading_data)
        }).then((response) =>  response.json()).then((json) => {


                console.log(json);
                //return(json);
                resolve(json);
             
              //setmessagedata(json);
        });
      
       }catch(err){
              //setmessagedata("");
              Alert.prompt('Testing', 'err' + err);
        }


      });

    

      }
    

        return(
            <View style={{justifyContent: 'center'}}>
            
            <ImageBackground  source={require('../images/wave.png')} style={styles.backgroundWave}>
            <View style={{justifyContent: 'center'}}>

            <Text style={styles.header}>LMS Tracker</Text>
            <Text style={styles.subheader}>By ISN Managed Services</Text>
            
            
             </View>
            <View style={styles.formbox}>
            <ScrollView   
            style={styles.scrollViewStyle}
            horizontal={false}
            showsHorizontalScrollIndicator={false}
            snapToInterval={311}
            pagingEnabled
            
            
            scrollEventThrottle={1}
          >


            <View style={styles.inputContainerdouble} >

             <View style={styles.inputBoxSmall}>   
            <Text style={styles.label}>Landed Date</Text>
            <TextInput
            
                style={styles.textInput}
                placeholder="Landed Date"
                placeholderTextColor={'lightgray'}
                maxLength={20}
                value={landed_date}
                mode={date}
                label="Landed Date"
                onChangeText={(text)=>{
                
                    setlanded_date(text);

              }}
            
                />
           </View>  
           <View style={styles.inputBox}> 
           <Text style={styles.label}>Landing Slip</Text>
           <TextInput
                style={styles.textInput}
                placeholder="Landing Slip"
                placeholderTextColor={'lightgray'}
                maxLength={20}
                value={landing_slip}
                
                onChangeText={(text)=>{
                
                setlanding_slip(text);

              }}
            
                />
            </View>  
            </View>


            <View style={styles.inputContainerdouble} >

             <View style={styles.inputBoxSmall}>   
            <Text style={styles.label}>Quota</Text>
            <TextInput
                style={styles.textInput}
                placeholder="Quota"
                placeholderTextColor={'lightgray'}
                maxLength={20}
                keyboardType={"phone-pad"}
                
                value={quota}
                onChangeText={setquota}
                onChangeText={(text)=>{
                
                    setquota(text);

              }}
            
                />
           </View>  
           <View style={styles.inputBox}> 
           <Text style={styles.label}>Quota Name</Text>
           <SelectDropdown
                data={quotaData}
                defaultButtonText={"Choose Quota Name"}
                buttonTextStyle={{fontSize:13, textAlign:'left', color:'#5271FF'}}
                buttonStyle={{borderColor:'#5271FF', borderRadius: 15, width:'100%', borderWidth:1,  height: 39,
            fontSize: 13,
            borderRadius:15,
            paddingLeft: 8,}}
                value={quota_name}
                onChangeText={setquota_name}
                onChangeText={(text)=>{
                
                setquota_name(text)}}
                onSelect={(selectedItem, index) => {
                    console.log(selectedItem, index)
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item
                }}
            />

          
            </View>  
            </View>

            <View style={styles.inputContainerdouble} >

            <View style={styles.inputBoxSmall}>   
            <Text style={styles.label}>Area</Text>
            <SelectDropdown
                data={areaData}
                defaultButtonText={"Choose an Area"}
                buttonTextStyle={{fontSize:13, textAlign:'left', color:'#5271FF'}}
                buttonStyle={{borderColor:'#5271FF', borderRadius: 15, width:'100%', borderWidth:1,  height: 39,
            fontSize: 13,
            borderRadius:15,
            paddingLeft: 8,}}
                value={area}
                onChangeText={setarea}
                onChangeText={(text)=>{
                
                setarea(text)}}
                onSelect={(selectedItem, index) => {
                    console.log(selectedItem, index)
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item
                }}
            />

           
            </View>  
            <View style={styles.inputBox}> 
            <Text style={styles.label}>Vessel</Text>

            <SelectDropdown
                data={vessselData}
                defaultButtonText={"Choose Vessel"}
                buttonTextStyle={{fontSize:13, textAlign:'left', color:'#5271FF'}}
                buttonStyle={{borderColor:'#5271FF', borderRadius: 15, width:'100%', borderWidth:1,  height: 39,
            fontSize: 13,
            borderRadius:15,
            paddingLeft: 8,}}
                value={vessel}
                onChangeText={setvessel}
                onChangeText={(text)=>{
                
                setvessel(text)}}
                onSelect={(selectedItem, index) => {
                    console.log(selectedItem, index)
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item
                }}
            />
          
          
            </View>  
            </View>


            <View style={styles.inputContainerdouble} >
            <View style={styles.inputBoxSmall}> 
            <Text style={styles.label}>Landing Site</Text>

            <SelectDropdown
                data={landingsiteData}
                defaultButtonText={"Choose Landing Site"}
                buttonTextStyle={{fontSize:13, textAlign:'left', color:'#5271FF'}}
                buttonStyle={{borderColor:'#5271FF', borderRadius: 15, width:'100%', borderWidth:1,  height: 39,
            fontSize: 13,
            borderRadius:15,
            paddingLeft: 8,}}
                value={landing_site}
                onChangeText={setlanding_site}
                onChangeText={(text)=>{
                
                setlanding_site(text)}}
                onSelect={(selectedItem, index) => {
                    console.log(selectedItem, index)
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item
                }}
            />
           
            </View>  

            <View style={styles.inputBox}> 
            <Text style={styles.label}>Harbour</Text>

            <SelectDropdown
                data={harbourData}
                defaultButtonText={"Choose Harbour"}
                buttonTextStyle={{fontSize:13, textAlign:'left', color:'#5271FF'}}
                buttonStyle={{borderColor:'#5271FF', borderRadius: 15, width:'100%', borderWidth:1,  height: 39,
            fontSize: 13,
            borderRadius:15,
            paddingLeft: 8,}}
                value={harbour}
                onChangeText={setharbour}
                onChangeText={(text)=>{
                
                setharbour(text)}}
                onSelect={(selectedItem, index) => {
                    console.log(selectedItem, index)
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                    // text represented after item is selected
                    // if data array is an array of objects then return selectedItem.property to render after item is selected
                    return selectedItem
                }}
                rowTextForSelection={(item, index) => {
                    // text represented for each item in dropdown
                    // if data array is an array of objects then return item.property to represent item in dropdown
                    return item
                }}
            />
           
            </View>  
            </View>
          
            <View style={styles.inputContainerdouble} >

                <View style={styles.inputBoxSmallerleft}>   
                    <Text style={styles.label}>Kgs Slip</Text>
                    <TextInput
                    style={styles.textInput}
                    placeholder="Kgs Slip"
                    placeholderTextColor={'lightgray'}
                    maxLength={20}
                    keyboardType={"phone-pad"}

                    value={kgs_slips}
                    onChangeText={setkgs_slips}
                    onChangeText={(text)=>{

                        setkgs_slips(text);

                    }}

                    />
                </View>  

                <View style={styles.inputBoxSmaller}> 
                    <Text style={styles.label}>Kgs Rec.</Text>
                    <TextInput
                    style={styles.textInput}
                    placeholder="Kgs Rec."
                    placeholderTextColor={'lightgray'}
                    maxLength={20}
                    value={kgs_received}

                    onChangeText={(text)=>{

                    setkgs_received(text);

                    }}

                    />

                </View>

                <View style={styles.inputBoxSmallerT}> 
                <Text style={styles.label}>Traps</Text>
                <TextInput
                style={styles.textInput}
                placeholder="Traps"
                placeholderTextColor={'lightgray'}
                maxLength={20}
                value={traps}

                onChangeText={(text)=>{

                settraps(text);

                }}

                />
                </View>  
                
                </View>


            <View style={styles.inputContainerdoubleIcons} >

            <View style={styles.inputBoxSmaller}>   
        
                            <TouchableOpacity
                             onPress={()=>{

                                 console.log('pressed camera');

                             }}
                            >
          
                            <Image

                            style={styles.camera}
                            
                            source={require('../images/cam2.png')}
                            />
                            </TouchableOpacity>
                    
             {/*   <Camera
                value={image}
                onChangeText={setimage}
                >

                </Camera>  */}
            </View>  

            <View style={styles.inputBox}> 
            <Geo value={gps}
                onChangeText={setgps}></Geo>  

            </View>

            </View>
          
            </ScrollView>

            </View>
            <View style={{justifyContent: 'center'}}>
            <TouchableOpacity
       
                style={styles.myButton}
                onPress={()=> {
        
          
                    console.log('here');

                    Alert.alert('posting');

                    //Get this from fields
                    let catch_json = {
                        "landing_slip": "1234",
                        "landed-date": "1234",
                        "quota": "1234",
                        "quota_name": "1234",
                        "area": "1234",
                        "vessel": "1234",
                        "kgs_slips": "1234",
                        "kgs_received": "1234",
                        "traps": "1234",
                        "landing_site": "1234",
                        "harbour": "1234",
                        "gps": "1234",
                        "image": "xxx"
                        }
                    post_landing_data(catch_json);
                  
                  }}
            >
                <LinearGradient colors={['#4AF3FA', '#5271FF', ]} style={styles.gradient} start={{ x: 1, y: 0 }} end={{ x: 0, y: 0 }}>
            
                
                <Text style={{color:'white', textAlign:'center', letterSpacing:3, fontSize:16, fontFamily: 'Roboto',}}>SUBMIT</Text>
                </LinearGradient>
      </TouchableOpacity>
      </View>
            </ImageBackground>
            </View>
            
            
            );

};

const styles = StyleSheet.create({

   

    inputContainerdoubleIcons:{
        paddingTop: 10,
        // marginBottom:50,
        // height:300,
         alignContent:'center',
         justifyContent:'center',
         alignItems:'center',
        width:'95%',
        // marginHorizontal:30,
         flexDirection:'row',
         alignSelf:'center',

      },
    camera:{
        width:40,
        height:40,
    },
    label:{
        color:'#5271FF',
        paddingBottom:5,
        paddingLeft: 8,
    },

    inputBoxSmall:{
        //backgroundColor:'red',
        width:'50%',
        paddingRight:10,
    },
    inputBoxSmaller:{
        //backgroundColor:'red',
        width:'33%',
        paddingRight:6,
        
    },
    inputBoxSmallerleft:{
        //backgroundColor:'red',
        width:'33%',
        paddingRight:6,
    },

    inputBoxSmallerT:{
        width:'33%',
        
    },

    inputBox:{
        //backgroundColor:'blue',
        width:'50%',
    },
    container:{
     // top:130,

    },

    scrollViewStyle:{
     //flexDirection: 'row',
    //flexWrap: 'nowrap',
    overflow: 'scroll',
    marginTop:'22%',
    
    },

    row:{
        flexDirection: 'row',
        marginTop:10,
    },
    datepicker:{
     
       maxHeight:150,
       maxWidth:250,
       marginBottom:30,
       

    },

    myButton:{
        alignItems:'center',

    },
  

    gradient: {
                marginTop:20,
                width:150,                
                borderRadius: 50,
                height: 50,
                justifyContent: 'center',
                textAlign:'center',
                
               
            },

            buttonText:{
                textAlign: 'center'
            },

        subheader:{
            fontSize:10,
            color:'white',
            textAlign:'right',
            marginRight:20,
        },


        formbox:{
          marginTop: 53,
          marginEnd:8,
          marginStart:8,
          
         
         justifyContent: 'center',

        },

        header:{

            fontSize:20,
            paddingTop:40,           
            fontFamily: 'Roboto',
            textTransform:'uppercase',
            color:'white',
            letterSpacing:3,
            textAlign: 'right',
            marginRight:20,
          
        },

        backgroundWave:{
            
           height:'100%',
            width:'100%',
            marginTop:-49,
           
           
            
            
        },

        inputContainerdouble: {
           // marginTop:70,
            paddingTop: 7,
           // marginBottom:50,
           // height:300,
            alignContent:'center',
            justifyContent:'center',
            alignItems:'center',
           width:'95%',
           // marginHorizontal:30,
            flexDirection:'row',
            alignSelf:'center',
          },
          textInput: {
            borderColor: '#5271FF',            
            borderWidth: 1,
            height: 39,
            fontSize: 13,
            borderRadius:15,
            paddingLeft: 8,
            //paddingRight: 20,
            textAlign:'left',
           // marginBottom:5,
            width:"100%",
            color:'#5271FF',
          },

          textInputSmall:{
            borderColor: '#5271FF',            
            borderWidth: 1,
            padding:10,
            height: 39,
            fontSize: 13,
           // marginRight:20,
            borderRadius:15,
           // paddingLeft: 20,
           // paddingRight: 20,
            textAlign:'center',
           // marginBottom:10,
            width:'100%',
            marginHorizontal:4,
            

          },

         

});

export default HomeScreen;