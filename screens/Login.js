import React, { useState, useEffect, Component } from "react";
import { Text,
     StyleSheet, 
     View,   
     Image,
    ImageBackground,
    ScrollView,
    TextInput,
    TouchableOpacity,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';



const LoginScreen = (props) => {

    


    const [toggleCheckBox, setToggleCheckBox] = useState(false)

    const [maccesstoken, setmaccesstoken ] = useState();

    useEffect(() => {
      // Run! Like go get some data from an API.

      console.log('Login Detected');

    }, [maccesstoken]);


    useEffect(() => {

      retrieveUserLogin();

    },[]);


    const [username, setusername] = useState();
    const [password, setpassword] = useState();


    const [apiserver, setapiserver] = useState();





    async function storeUserLogin() {
      try {
          await EncryptedStorage.setItem(
              "user_login",
              JSON.stringify({
                  username : username,
                  password : password
              })
          );
  
          // Congrats! You've just stored your first value!
          
      } catch (error) {
          // There was an error on the native side
      }
  }


  async function retrieveUserLogin() {
    try {   
        const user_login = JSON.parse(await EncryptedStorage.getItem("user_login"));
    
        if (user_login !== undefined) {
            // Congrats! You've just retrieved your first value!
            setusername(user_login.username);
            setpassword(user_login.password);
            
        }
    } catch (error) {
        // There was an error on the native side
    }
}


async function clearStorage() {
  try {
      await EncryptedStorage.clear();
      Alert.alert('Storage Cleared');
      // Congrats! You've just cleared the device storage!
  } catch (error) {
      // There was an error on the native side
  }
}


    async function getItem(data){


        try {
            const value = await AsyncStorage.getItem('laccesstoken');
            if (value !== null) {
             

              setmaccesstoken(value);
           

              return(value);
            }
          } catch (error) {
            // Error retrieving data
            return(null);
          }
   
     }
    
     async function setItem(data){
    
    
        try {
            const value = await AsyncStorage.setItem('laccesstoken', data);
            if (value !== null) {
              // We have data!!
              console.log(value);
              //return(value);
            }
          } catch (error) {
            // Error retrieving data
            //return(null);
            console.log(error);
          }
    
    
     }


      try{
        

      }
      catch(err){
          console.log(err);
      }



      [resp,setresp] = useState();
      
            

        return(
            <View style={{justifyContent: 'center'}}>
            
            <ImageBackground  source={require('../images/background.png')} style={styles.background}>
            <View style={{justifyContent: 'center'}}>

            <Text style={styles.header}>LMS Tracker</Text>
            <Text style={styles.subheader}>By ISN Managed Services</Text>
            
            
             </View>
            <View style={styles.formbox}>
            <Text style={styles.header2}>LOGIN</Text>
            <ScrollView>
            <View style={styles.inputContainer}>
            
            <TextInput
                style={styles.textInput}
                placeholder="Username"
                placeholderTextColor={'white'}
                maxLength={20}
            
                />
                 <TextInput
                style={styles.textInput}
                placeholder="Password"
                placeholderTextColor={'white'}
                maxLength={20}
                />
                
            </View>
            </ScrollView>

            </View>
            <View style={{justifyContent: 'center', alignItems:'center'}}>
            <TouchableOpacity
       
                style={styles.myButton}
                onPress={() => {
                    const navigation = useNavigation();


const [toggleCheckBox, setToggleCheckBox] = useState(false)

const [maccesstoken, setmaccesstoken ] = useState();

useEffect(() => {
  // Run! Like go get some data from an API.

  console.log('Login Detected');

}, [maccesstoken]);


useEffect(() => {

  retrieveUserLogin();

},[]);


const [username, setusername] = useState();
const [password, setpassword] = useState();


const [apiserver, setapiserver] = useState();





async function storeUserLogin() {
  try {
      await EncryptedStorage.setItem(
          "user_login",
          JSON.stringify({
              username : username,
              password : password
          })
      );

      // Congrats! You've just stored your first value!
      
  } catch (error) {
      // There was an error on the native side
  }
}


async function retrieveUserLogin() {
try {   
    const user_login = JSON.parse(await EncryptedStorage.getItem("user_login"));

    if (user_login !== undefined) {
        // Congrats! You've just retrieved your first value!
        setusername(user_login.username);
        setpassword(user_login.password);
        
    }
} catch (error) {
    // There was an error on the native side
}
}


async function clearStorage() {
try {
  await EncryptedStorage.clear();
  Alert.alert('Storage Cleared');
  // Congrats! You've just cleared the device storage!
} catch (error) {
  // There was an error on the native side
}
}


async function getItem(data){


    try {
        const value = await AsyncStorage.getItem('laccesstoken');
        if (value !== null) {
         

          setmaccesstoken(value);
       

          return(value);
        }
      } catch (error) {
        // Error retrieving data
        return(null);
      }

 }

 async function setItem(data){


    try {
        const value = await AsyncStorage.setItem('laccesstoken', data);
        if (value !== null) {
          // We have data!!
          console.log(value);
          //return(value);
        }
      } catch (error) {
        // Error retrieving data
        //return(null);
        console.log(error);
      }


 }







  try{
    

  }
  catch(err){
      console.log(err);
  }


                
  [resp,setresp] = useState();
                }
                }
                
            >
               
            
                
                <Text style={{color:'#4DCBFC', textAlign:'center', letterSpacing:3, fontSize:18, fontFamily: 'Raleway',}}>LOGIN</Text>
               
      </TouchableOpacity>
      </View>
            </ImageBackground>
            </View>
            
            
            );

};

const styles = StyleSheet.create({

    header2:{

        fontSize:25,                  
            fontFamily: 'Raleway',
            textTransform:'uppercase',
            color:'white',
            letterSpacing:3,
            textAlign: 'center',
            marginBottom:20,

    },

    myButton:{
   
        backgroundColor:'white',
        marginTop:20,
        width:170,                
        borderRadius: 50,
        height: 50,
        justifyContent: 'center',
        textAlign:'center',
        alignItems:'center',

    },
  


          

        subheader:{
            fontSize:12,
            color:'white',
            textAlign:'center',
         
        },


        formbox:{
          marginTop:120,
          marginEnd:30,
          marginStart:30,
          
         
         justifyContent: 'center',

        },

        header:{

            fontSize:28,
            fontWeight:'bold',
            paddingTop:70,           
            fontFamily: 'Raleway',
            textTransform:'uppercase',
            color:'white',
            letterSpacing:3,
            textAlign: 'center',
            
          
        },

        background:{
            
           height:'100%',
            width:'100%',
        
           
           
            
            
        },

        inputContainer: {
            paddingTop: 10,
          },
          textInput: {
            borderColor: 'white',            
            borderBottomWidth: 1,
            height: 60,
            fontSize: 15,
            paddingLeft: 20,
            paddingRight: 20,
            textAlign:'center',
            marginBottom:5,
            color:'white',
          }

});

export default LoginScreen;