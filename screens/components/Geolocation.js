import React, { Component } from 'react';
import {
    Platform, StyleSheet, Text, View,
    Button, Alert,
    ActivityIndicator,
    TextInput,
    TouchableOpacity,
    Image,
} from 'react-native';

import GetLocation from 'react-native-get-location';



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
        flexDirection:'row',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    location: {
        color: '#333333',
        marginBottom: 5,
    },
    button: {
        marginBottom: 8,
    },
    textInputSmall: {

        borderColor: '#5271FF',            
        borderWidth: 1,
        padding:10,
        height: 50,
        fontSize: 13,
        paddingLeft: 20,
        paddingRight: 20,
        textAlign:'center',
        marginBottom:10,
        marginHorizontal:4,
        width:250,

    },
    textInput: {
        borderColor: '#5271FF',            
        borderWidth: 1,
        height: 39,
        fontSize: 13,
        borderRadius:15,
        paddingLeft: 8,
        //paddingRight: 20,
        textAlign:'left',
       // marginBottom:5,
        width:"100%",
        color:'#5271FF',
      },

      gps:{

        width:40,
        height:40,
        marginRight:8,
        marginTop:7,
    
      },
   
});

export default class Geo extends Component {

    state = {
        location: null,
        loading: false,
    }

     _requestLocation = () => {
        this.setState({ loading: true, location: null });

        GetLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 150000,
        })
            .then(location => {
                this.setState({
                    location,
                    loading: false,
                });
            })
            .catch(ex => {
                const { code, message } = ex;
                console.warn(code, message);
                if (code === 'CANCELLED') {
                    Alert.alert('Location cancelled by user or by another request');
                }
                if (code === 'UNAVAILABLE') {
                    Alert.alert('Location service is disabled or unavailable');
                }
                if (code === 'TIMEOUT') {
                    Alert.alert('Location request timed out');
                }
                if (code === 'UNAUTHORIZED') {
                    Alert.alert('Authorization denied');
                }
                this.setState({
                    location: null,
                    loading: false,
                });
            });
    }

    render() {
        const { location, loading } = this.state;
        return (
            <View style={styles.container}>
                
               
                
                <View style={styles.button}>
                    <TouchableOpacity
                        disabled={loading}
                       
                        title="Get Location"
                        onPress={this._requestLocation}
                    >
                     <Image

                        style={styles.gps}

                        source={require('../../images/gps.png')}
                        />
                        </TouchableOpacity>
                  
                </View>
                {loading ? (
                    <ActivityIndicator />
                ) : null}
                {location ? (
                    <TextInput
                      style={styles.textInput}
                placeholder="GPS"
                placeholderTextColor={'#5271FF'}
                maxLength={400}>
                        {JSON.stringify(location, 0, 2)}
                    </TextInput>
                ) : null}

                {/*
                <Text style={styles.instructions}>Extra functions:</Text>
                <View style={styles.button}>
                    <Button
                        title="Open App Settings"
                        onPress={() => {
                            GetLocation.openAppSettings();
                        }}
                    />
                </View>
                <View style={styles.button}>
                    <Button
                        title="Open Gps Settings"
                        onPress={() => {
                            GetLocation.openGpsSettings();
                        }}
                    />
                </View>
                <View style={styles.button}>
                    <Button
                        title="Open Wifi Settings"
                        onPress={() => {
                            GetLocation.openWifiSettings();
                        }}
                    />
                </View>
                <View style={styles.button}>
                    <Button
                        title="Open Mobile Data Settings"
                        onPress={() => {
                            GetLocation.openCelularSettings();
                        }}
                    />
                </View>*/}
               
            </View>
        );
    }
}