import React, { useState } from 'react';
import { View, Text } from 'react-native';

import Picker from '@gregfrench/react-native-wheel-picker'
var PickerItem = Picker.Item;

const AreaPicker = () => {
  const [selectedItem, setSelectedItem ] = useState(2);
  const [itemList , setItemList ] = useState(['3', '6','8', '11', '12']);

  return (
    <View>
      <Text>
        <Picker style={{width: 250, height: 200}}
          lineColor="#000000" //to set top and bottom line color (Without gradients)
          lineGradientColorFrom="#4AF3FA" //to set top and bottom starting gradient line color
          lineGradientColorTo="#5271FF" //to set top and bottom ending gradient
          selectedValue={selectedItem}
          itemStyle={{color:"black", fontSize:20}}
          onValueChange={(index) => setSelectedItem(index) }>
          {itemList.map((value, i) => (
            <PickerItem label={value} value={i} key={i}/>
          ))}
        </Picker>
      </Text>
    </View>
  );
};

export default AreaPicker;
