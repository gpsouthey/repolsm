/**
   * Sample React Native App
   * https://github.com/facebook/react-native
   * @flow
   */
  
 'use strict';
 import React, { Component } from 'react';
 import {
   AppRegistry,
   Dimensions,
   Image,
   StyleSheet,
   Text,
   TouchableOpacity,
   View
 } from 'react-native';
 import { RNCamera } from 'react-native-camera';
 
 export default class Camera extends Component {

   constructor(props) {
     super(props);
     this.state = {
       uri: ''
     }
   }
     
   takePicture = async function() {
     if (this.camera) {
       const options = { quality: 0.5, base64: true };
       const data = await this.camera.takePictureAsync(options)
       let state = this.state;
       state.uri = data.uri;
       this.setState(state);
     }
   }    
     
   render() {
     return (
       <View style={styles.container}>
         <RNCamera
             ref={ref => {
               this.camera = ref;
             }}
             style = {styles.preview}
             type={RNCamera.Constants.Type.back}
             flashMode={RNCamera.Constants.FlashMode.on}
             captureAudio={false}
             
         />
         <View style={{flex: 0, flexDirection: 'row', justifyContent: 'center',}}>
         <TouchableOpacity
             onPress={this.takePicture.bind(this)}
             style = {styles.capture}
        >
             <Image style={{height: 50, width: 50}} source={require('./../../images/cam1.png')}/>
         </TouchableOpacity>
         </View>
         <View style={{height: 150, width: 150}}>
           <Image style={{height: 30, width: 30}} source={{uri:('')}}/>
         </View>
       </View>
     );
   }

   takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      console.log(data.uri);
    }
  };

 }
 
 const styles = StyleSheet.create({
   container: {
     flex: 1,
     flexDirection: 'column',
     width:250,
     height:350,
   },
   preview: {
     flex: 1,
     justifyContent: 'flex-end',
     alignItems: 'center'
   },
   capture: {
     flex: 0,
     
     borderRadius: 5,
     padding: 15,
     paddingHorizontal: 20,
     alignSelf: 'center',
     margin: 20
   }
 });