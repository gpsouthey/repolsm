import 'react-native-gesture-handler';
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';

import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from 'react-native';

import HomeScreen from './screens/Home';
import LoginScreen from './screens/Login';
import { useNavigation } from '@react-navigation/native';
import { NavigationContainer } from '@react-navigation/native';
//import MyStack from './screens/components/Stack';



const App = () => {
 


 
 

  return (
    <NavigationContainer >
    

    <SafeAreaView>
      <View>
        <HomeScreen/>
      </View>
    </SafeAreaView>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({
 
});

export default App;
